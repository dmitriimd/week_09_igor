package ru.edu.task5.java;

import org.springframework.beans.BeansException;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;

import ru.edu.task5.common.InterfaceToRemove;
import ru.edu.task5.common.InterfaceToWrap;

@Configuration
public class CustomBeanPostProcessor implements BeanPostProcessor {

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof InterfaceToWrap) {

            return new FunctionWrapper((InterfaceToWrap) bean);
        }

        if (bean instanceof InterfaceToRemove) {
            return "";
        }

        return bean;
    }

    private class FunctionWrapper implements InterfaceToWrap {
        private String WRAPPED = "wrapped ";
        private String resultValue;

        public FunctionWrapper(InterfaceToWrap implInterface) {
            this.resultValue = WRAPPED + implInterface.getClass();
        }

        @Override
        public String getValue() {
            return resultValue;
        }
    }

}

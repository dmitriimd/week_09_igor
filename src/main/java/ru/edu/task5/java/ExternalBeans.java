package ru.edu.task5.java;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.edu.task5.common.ImplementOne;
import ru.edu.task5.common.ImplementToDeleteOne;
import ru.edu.task5.common.ImplementToDeleteTwo;
import ru.edu.task5.common.ImplementTwo;

/**
 * ReadOnly
 */
@Configuration
public class ExternalBeans {

    @Bean
    public ImplementOne implementOne() {
        return new ImplementOne();
    }

    @Bean
    public ImplementTwo implementTwo() {
        return new ImplementTwo();
    }

    @Bean
    public ImplementToDeleteOne implementToDeleteOne() {
        return new ImplementToDeleteOne();
    }

    @Bean
    public ImplementToDeleteTwo implementToDeleteTwo() {
        return new ImplementToDeleteTwo();
    }

}

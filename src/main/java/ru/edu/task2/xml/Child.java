package ru.edu.task2.xml;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * ReadOnly
 */
public class Child {

    @Autowired
    private TimeKeeper timeKeeper;

    public TimeKeeper getTimeKeeper() {
        return timeKeeper;
    }

}
